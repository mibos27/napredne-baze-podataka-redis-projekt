# Napredne baze podataka - Redis projekt (Riak baza nije funkcionirala)

Baza podataka treba omogućavati pohranu podataka o prijavljenim korisnicima. Svaki korisnik može
unijeti osobne podatke, sliku te spremiti svoje preferencije koje se odnose na primanje raznih vrsta
obavijesti iz sustava. Modelirati način pohrane podataka. Koristiti Riak. Napuniti podatke za 30
korisnika (generirati po potrebi).
Izgraditi aplikaciju (u programskom jeziku/okruženju po želji) u koju će se korisnik moći jednom
registrirati i koja će mu dohvatiti njegove podatke ako je već registriran.. 
