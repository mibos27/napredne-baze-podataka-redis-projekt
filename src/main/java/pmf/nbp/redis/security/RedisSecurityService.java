package pmf.nbp.redis.security;

import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;
import org.springframework.stereotype.Component;
import pmf.nbp.redis.model.AppUser;
import pmf.nbp.redis.repository.UserRepository;
import pmf.nbp.redis.util.UserRoles;

@Component("RedisSecurityService")
public class RedisSecurityService {

    private final UserRepository userRepository;

    @Autowired
    public RedisSecurityService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean hasRole(final String role) {
        AppUser user = (AppUser) getContext().getAuthentication().getPrincipal();

        boolean pass = user.getRole().equals(UserRoles.valueOf(role));

        if (!pass) {
            user = userRepository.getAppUser(user.getUsername());
            pass = user.getRole().equals(UserRoles.valueOf(role));
        }

        return pass;
    }

}
