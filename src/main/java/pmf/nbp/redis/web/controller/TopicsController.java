package pmf.nbp.redis.web.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pmf.nbp.redis.model.AppUser;
import pmf.nbp.redis.model.Topics;
import pmf.nbp.redis.service.TopicsService;
import pmf.nbp.redis.service.UserService;

@Controller
public class TopicsController {

    private final UserService userService;

    private final TopicsService topicsService;

    @Autowired
    public TopicsController(UserService userService, TopicsService topicsService) {
        this.userService = userService;
        this.topicsService = topicsService;
    }

    @GetMapping("/admin-topics")
    public String adminTopics(Model model, @AuthenticationPrincipal AppUser user) {
        AppUser currentUser = userService.getUserByUsername(user.getUsername());
        model.addAttribute("user", currentUser);
        model.addAttribute("topicsList", topicsService.getAllTopics());
        model.addAttribute("topicsListEmpty", new Topics());
        model.addAttribute("newTopics", new Topics());
        return "admin";
    }

    @PostMapping("/delete-topics")
    public String deleteTopics(@ModelAttribute("topics") Topics topics) {
        topicsService.deleteListTopics(List.copyOf(topics.getTopics()));
        return "redirect:/admin-topics";
    }

    @PostMapping("/add-topic")
    public String addTopic(@ModelAttribute("newTopic") Topics topics) {
        topicsService.addListTopics(List.copyOf(topics.getTopics()));
        return "redirect:/admin-topics";
    }
}
