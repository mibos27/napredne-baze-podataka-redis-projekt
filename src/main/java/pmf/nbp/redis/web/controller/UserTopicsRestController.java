package pmf.nbp.redis.web.controller;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pmf.nbp.redis.model.Topics;
import pmf.nbp.redis.service.UserTopicsService;
import pmf.nbp.redis.web.dto.UserTopicDto;

@RestController
@RequestMapping("/user-topics")
public class UserTopicsRestController {

    private final UserTopicsService userTopicsService;

    public UserTopicsRestController(UserTopicsService userTopicsService) {
        this.userTopicsService = userTopicsService;
    }

    @GetMapping("/getAllUserTopics/{username}")
    public ResponseEntity<Topics> getAllUserTopics(@PathVariable String username) {
        Topics allUserTopics = userTopicsService.getAllUserTopics(username);
        return new ResponseEntity<>(allUserTopics, HttpStatus.OK);
    }

    @PostMapping("/addOneUserTopic/{username}")
    public ResponseEntity<Topics> addOneUserTopic(@PathVariable String username, @RequestBody String topicName) {
        userTopicsService.addOneUserTopic(username, topicName);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/addListUserTopics")
    public ResponseEntity<Topics> addUserTopics(@RequestBody UserTopicDto userTopicDto) {
        userTopicsService.addListUserTopics(userTopicDto.getUsername(), List.copyOf(userTopicDto.getTopics()));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/deleteOneUserTopic/{username}")
    public ResponseEntity<Topics> deleteOneUserTopic(@PathVariable String username, @RequestBody String topicName) {
        userTopicsService.deleteOneUserTopic(username, topicName);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/deleteListUserTopics")
    public ResponseEntity<Topics> deleteListUserTopics(@RequestBody UserTopicDto userTopicDto) {
        userTopicsService.deleteListUserTopics(userTopicDto.getUsername(), List.copyOf(userTopicDto.getTopics()));
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
