package pmf.nbp.redis.web.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pmf.nbp.redis.model.AppUser;
import pmf.nbp.redis.model.Topics;
import pmf.nbp.redis.model.UserRegistrationForm;
import pmf.nbp.redis.service.TopicsService;
import pmf.nbp.redis.service.UserService;
import pmf.nbp.redis.service.UserTopicsService;
import pmf.nbp.redis.util.Encoder;
import pmf.nbp.redis.util.UserRoles;
import pmf.nbp.redis.web.dto.UserNotificationsDto;

@Controller
public class MainController {

    private final UserService userService;

    private final TopicsService topicsService;

    private final UserTopicsService userTopicsService;

    @Autowired
    public MainController(UserService userService, TopicsService topicsService,
            UserTopicsService userTopicsService) {
        this.userService = userService;
        this.topicsService = topicsService;
        this.userTopicsService = userTopicsService;
    }

    @GetMapping("/")
    public String index(Model model, @AuthenticationPrincipal AppUser user) {
        if (user == null) {
            return "redirect:/login";
        }
        AppUser currentUser;
        if (user.getRole().equals(UserRoles.ADMIN)) {
            return "redirect:/admin-topics";
        } else {
            currentUser = userService.getUserByUsername(user.getUsername());
        }

        model.addAttribute("user", currentUser);
        model.addAttribute("changeUser", new UserRegistrationForm());
        return "home";
    }

    @GetMapping("/notifications")
    public String notifications(Model model, @AuthenticationPrincipal AppUser appUser) {
        AppUser currentUser = userService.getUserByUsername(appUser.getUsername());
        model.addAttribute("user", currentUser);
        model.addAttribute("notifications", new UserNotificationsDto(currentUser.isSendNotifications(),
                currentUser.isAllowDataCollection(), currentUser.isNewTopicNotification()));
        return "notifications";
    }

    @PostMapping("/update-notifications")
    public String updateNotifications(@ModelAttribute("notifications") UserNotificationsDto notifications,
            @AuthenticationPrincipal AppUser current) {
        current.setSendNotifications(notifications.isSendNotifications());
        current.setNewTopicNotification(notifications.isNewTopicNotification());
        current.setAllowDataCollection(notifications.isAllowDataCollection());
        userService.updateUser(current);
        return "redirect:/notifications";
    }

    @GetMapping("/topics")
    public String userTopics(Model model, @AuthenticationPrincipal AppUser appUser) {
        AppUser currentUser = userService.getUserByUsername(appUser.getUsername());
        model.addAttribute("user", currentUser);
        model.addAttribute("userTopics", userTopicsService.getAllUserTopics(currentUser.getUsername()));
        model.addAttribute("availableTopics", userTopicsService.getDiff(currentUser.getUsername()));
        model.addAttribute("follow", new Topics());
        model.addAttribute("unfollow", new Topics());
        return "topics";
    }

    @PostMapping("/follow-topics")
    public String followTopics(@ModelAttribute("follow") Topics follow, @AuthenticationPrincipal AppUser appUser) {
        userTopicsService.addListUserTopics(appUser.getUsername(), List.copyOf(follow.getTopics()));
        return "redirect:/topics";
    }

    @PostMapping("/unfollow-topics")
    public String unfollowTopics(@ModelAttribute("unfollow") Topics unfollow, @AuthenticationPrincipal AppUser appUser) {
        userTopicsService.deleteListUserTopics(appUser.getUsername(), List.copyOf(unfollow.getTopics()));
        return "redirect:/topics";
    }

    @PostMapping("/upload-image")
    public String uploadImage(@RequestParam("file") MultipartFile image, @AuthenticationPrincipal AppUser user)
            throws IOException {
        userService.updateImage(Encoder.getEncodedString(image.getBytes()), user);
        return "redirect:/";
    }

    @PostMapping("/change-password")
    public String changePassword(@ModelAttribute("changeUser") UserRegistrationForm changeUser, @AuthenticationPrincipal AppUser appUser) {
        if(changeUser.getPassword() != changeUser.getMatchingPassword()) {
            
        }
        userService.changePassword(changeUser.getPassword(), appUser);
        return "redirect:/";
    }
}
