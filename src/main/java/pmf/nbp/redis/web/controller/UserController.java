package pmf.nbp.redis.web.controller;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pmf.nbp.redis.model.LoginForm;
import pmf.nbp.redis.model.UserRegistrationForm;
import pmf.nbp.redis.service.UserService;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String prepareLogin(Model model) {
        model.addAttribute("loginForm", new LoginForm());
        return "login";
    }

    @GetMapping("/logout")
    public String logout() {
        return "redirect:/login?logout=true";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("user", new UserRegistrationForm());
        return "registration";
    }

    @PostMapping("/register")
    public String validateRegistration(@ModelAttribute("user") UserRegistrationForm registrationForm
            , BindingResult result) throws IOException {
        if (userService.userExists(registrationForm.getUsername())) {
            result.rejectValue("username", null, "That username already exists.");
        }
        if (!registrationForm.getPassword().equals(registrationForm.getMatchingPassword())) {
            result.rejectValue("matchingPassword", null, "Passwords do not match.");
        }
        if (result.hasErrors()) {
            return "registration";
        }

        userService.createUser(registrationForm);
        return "redirect:/login";
    }

}
