package pmf.nbp.redis.web.controller;

import java.io.IOException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pmf.nbp.redis.model.AppUser;
import pmf.nbp.redis.model.UserRegistrationForm;
import pmf.nbp.redis.service.UserService;

@RestController
@RequestMapping("/user")
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/get-by-username/{username}")
    public ResponseEntity<AppUser> getByUsername(@PathVariable String username) {
        AppUser appUser = userService.getUserByUsername(username);
        return new ResponseEntity<>(appUser, HttpStatus.OK);
    }

    @PostMapping("/add-user")
    public ResponseEntity<AppUser> addUser(@RequestBody UserRegistrationForm userRegistrationForm) throws IOException {
        userService.createUser(userRegistrationForm);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
