package pmf.nbp.redis.web.controller;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pmf.nbp.redis.model.Topics;
import pmf.nbp.redis.service.TopicsService;

@RestController
@RequestMapping("/topics")
public class TopicsRestController {

    private final TopicsService topicsService;

    public TopicsRestController(TopicsService topicsService) {
        this.topicsService = topicsService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<Topics> getAllTopics() {
        Topics allTopics = topicsService.getAllTopics();
        return new ResponseEntity<>(allTopics, HttpStatus.OK);
    }

    @PostMapping("/addOne")
    public ResponseEntity<Topics> addOneTopic(@RequestBody String topicName) {
        topicsService.addOneTopic(topicName);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/addList")
    public ResponseEntity<Topics> addTopics(@RequestBody List<String> topics) {
        topicsService.addListTopics(topics);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/deleteOne")
    public ResponseEntity<Topics> deleteOneTopic(@RequestBody String topicName) {
        topicsService.deleteOneTopic(topicName);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/deleteList")
    public ResponseEntity<Topics> deleteList(@RequestBody List<String> topics) {
        topicsService.deleteListTopics(topics);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
