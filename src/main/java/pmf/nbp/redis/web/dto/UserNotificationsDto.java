package pmf.nbp.redis.web.dto;

public class UserNotificationsDto {

    private boolean sendNotifications;

    private boolean allowDataCollection;

    private boolean newTopicNotification;

    public UserNotificationsDto() {
    }

    public UserNotificationsDto(boolean sendNotifications, boolean allowDataCollection,
            boolean newTopicNotification) {
        this.sendNotifications = sendNotifications;
        this.allowDataCollection = allowDataCollection;
        this.newTopicNotification = newTopicNotification;
    }

    public boolean isSendNotifications() {
        return sendNotifications;
    }

    public void setSendNotifications(boolean sendNotifications) {
        this.sendNotifications = sendNotifications;
    }

    public boolean isAllowDataCollection() {
        return allowDataCollection;
    }

    public void setAllowDataCollection(boolean allowDataCollection) {
        this.allowDataCollection = allowDataCollection;
    }

    public boolean isNewTopicNotification() {
        return newTopicNotification;
    }

    public void setNewTopicNotification(boolean newTopicNotification) {
        this.newTopicNotification = newTopicNotification;
    }

}
