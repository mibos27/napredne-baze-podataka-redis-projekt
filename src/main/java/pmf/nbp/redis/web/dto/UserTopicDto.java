package pmf.nbp.redis.web.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class UserTopicDto {

    private String username;

    private Set<String> topics;

    public UserTopicDto(String username, List<String> topics) {
        this.username = username;
        this.topics = new HashSet<>(topics);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserTopicDto)) {
            return false;
        }
        UserTopicDto userTopicDto = (UserTopicDto) o;
        return Objects.equals(getUsername(), userTopicDto.getUsername()) &&
                Objects.equals(getTopics(), userTopicDto.getTopics());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername(), getTopics());
    }

}
