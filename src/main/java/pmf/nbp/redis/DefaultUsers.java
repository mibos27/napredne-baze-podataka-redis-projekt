package pmf.nbp.redis;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import pmf.nbp.redis.model.AppUser;
import pmf.nbp.redis.model.UserRegistrationForm;
import pmf.nbp.redis.service.UserService;
import pmf.nbp.redis.util.UserRoles;

@Component
public class DefaultUsers implements ApplicationListener<ApplicationReadyEvent> {

    private final UserService userService;

    @Autowired
    public DefaultUsers(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        try {
            createUser("admin", "Admin", "Admin", "admin1234", "admin@admin.com", UserRoles.ADMIN);
            createUser("testUser", "Test", "User", "user1234", "test@user.com", UserRoles.USER);
            createUser("ihorv", "Ivan", "Horvat", "ihorv1234", "ihorv@user.com", UserRoles.USER);
            createUser("mibos", "Mihaela", "Bos", "12345", "mibos27@gmail.com", UserRoles.USER);
            createUser("enmajin", "Ena", "Majin", "12345", "majine95@gmail.com", UserRoles.USER);
            createUser("vbilogre", "Vlado", "Bilogrević", "12345", "vbilogre@user.com", UserRoles.USER);
            createUser("malujo", "Marija", "Lujo", "12345", "malujo@user.com", UserRoles.USER);
            createUser("jokov", "Josipa", "Kovačević", "jokov1234", "jokov@user.com", UserRoles.USER);
            createUser("ababic", "Ana", "Babić", "ababic1234", "ababic@user.com", UserRoles.USER);
            createUser("leam", "Lea", "Marić", "leam1234", "leam@user.com", UserRoles.USER);
            createUser("pjuri", "Petar", "Jurić", "pjuri1234", "pjuric@user.com", UserRoles.USER);
            createUser("vannov", "Vanja", "Novak", "vannov1234", "vannov@user.com", UserRoles.USER);
            createUser("mknez", "Mario", "Knežević", "mknez1234", "mknez@user.com", UserRoles.USER);
            createUser("mare12", "Marija", "Kovačić", "mare121234", "mare12@user.com", UserRoles.USER);
            createUser("jele34", "Jelena", "Vuković", "jele341234", "jele34@user.com", UserRoles.USER);
            createUser("anapetr", "Anamaria", "Petrović", "anapetr1234", "anapetr@user.com", UserRoles.USER);
            createUser("joza21", "Josip", "Marković", "joza211234", "joza21@user.com", UserRoles.USER);
            createUser("tanja56", "Tanja", "Devis", "tanja561234", "tanja56@user.com", UserRoles.USER);
            createUser("ivo95", "Ivica", "Jakobović", "ivo951234", "ivo95@user.com", UserRoles.USER);
            createUser("luce01", "Lucija", "Pavić", "luce011234", "luce01@user.com", UserRoles.USER);
            createUser("kata84", "Katarina", "Marić", "kata841234", "kata84@user.com", UserRoles.USER);
            createUser("maja63", "Maja", "Majin", "maja631234", "maja63@user.com", UserRoles.USER);
            createUser("stipa62", "Stjepan", "Delić", "stipa621234", "stipa62@user.com", UserRoles.USER);
            createUser("mirela63", "Mirela", "Dražetić", "mirela631234", "mirela63@user.com", UserRoles.USER);
            createUser("antoni83", "Antonio", "Raguž", "antoni831234", "antoni83@user.com", UserRoles.USER);
            createUser("jamale73", "Jamal", "Blažević", "jamale731234", "jamale73@user.com", UserRoles.USER);
            createUser("habaku", "Habaku", "Hill", "habaku1234", "habaku@user.com", UserRoles.USER);
            createUser("patrisa", "Patricia", "Serdić", "patrisa1234", "patrisa@user.com", UserRoles.USER);
            createUser("mart64", "Martin", "Obradović", "mart641234", "mart64@user.com", UserRoles.USER);
            createUser("darko4", "Darko", "Adžić", "darko41234", "darko4@user.com", UserRoles.USER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createUser(String username, String firstname, String lastname, String password, String email, UserRoles role)
            throws IOException {

        UserRegistrationForm user = new UserRegistrationForm(username, password, password, email, firstname, lastname);
        userService.createUser(user);
        AppUser appUser = userService.getUserByUsername(username);
        appUser.setRole(role);
        userService.updateUser(appUser);
    }
}
