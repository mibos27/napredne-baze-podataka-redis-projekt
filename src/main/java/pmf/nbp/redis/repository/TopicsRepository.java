package pmf.nbp.redis.repository;

import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Repository;
import pmf.nbp.redis.model.Topics;
import redis.clients.jedis.Jedis;

@Repository
public class TopicsRepository {

    public static final String KEY = "TOPICS";

    private final Jedis jedis;

    public TopicsRepository(Jedis jedis) {
        this.jedis = jedis;
    }

    public Topics getTopics() {
        Topics existingTopics = new Topics();
        Set<String> values = jedis.smembers(KEY);
        existingTopics.setTopics(values);

        return existingTopics;
    }

    public Topics addTopic(String topicName) {
        jedis.sadd(KEY, topicName);

        return new Topics(jedis.smembers(KEY));
    }

    public Topics addTopics(List<String> topics) {

        for (String topicName : topics) {
            jedis.sadd(KEY, topicName);
        }

        return new Topics(jedis.smembers(KEY));
    }

    public Long deleteTopic(String topicName) {
        return jedis.srem(KEY, topicName);
    }

    public void deleteTopics(List<String> topics) {
        for (String topicName : topics) {
            jedis.srem(KEY, topicName);
        }
    }

    public boolean topicIsMember(String topicName) {
        return jedis.sismember(KEY, topicName);
    }

}
