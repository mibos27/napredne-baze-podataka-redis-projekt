package pmf.nbp.redis.repository;

import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Repository;
import pmf.nbp.redis.model.Topics;
import redis.clients.jedis.Jedis;

@Repository
public class UserTopicsRepository {

    public static final String KEY       = "TOPICS:USER";
    public static final String KEYTOPICS = "TOPICS";

    private final Jedis jedis;

    public UserTopicsRepository(Jedis jedis) {
        this.jedis = jedis;
    }

    public Topics getUserTopics(String username) {
        Topics existingTopics = new Topics();
        Set<String> values = jedis.smembers(KEY + ":" + username);
        existingTopics.setTopics(values);

        return existingTopics;
    }

    public Topics addUserTopic(String username, String topicName) {
        jedis.sadd(KEY + ":" + username, topicName);

        return new Topics(jedis.smembers(KEY + ":" + username));
    }

    public Topics addUserTopics(String username, List<String> topics) {

        for (String topicName : topics) {
            jedis.sadd(KEY + ":" + username, topicName);
        }

        return new Topics(jedis.smembers(KEY + ":" + username));
    }

    public Long deleteUserTopic(String username, String topicName) {
        return jedis.srem(KEY + ":" + username, topicName);
    }

    public void deleteUserTopics(String username, List<String> topics) {
        for (String topicName : topics) {
            jedis.srem(KEY + ":" + username, topicName);
        }
    }

    public Set<String> getAllUserTopics() {
        return jedis.keys("*" + KEY + "*");
    }

    public boolean isMember(String topicName, String key) {
        return jedis.sismember(key, topicName);
    }

    public Long deleteWithKey(String key, String topicName) {
        return jedis.srem(key, topicName);
    }

    public Set<String> getDiff(String username) {
        return jedis.sdiff(KEYTOPICS, KEY + ":" + username);
    }

}
