package pmf.nbp.redis.repository;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import pmf.nbp.redis.model.AppUser;
import pmf.nbp.redis.model.UserRegistrationForm;
import pmf.nbp.redis.util.UserRoles;
import redis.clients.jedis.Jedis;

@Repository
public class UserRepository {

    public static final String KEY                    = "USER";
    public static final String PASSWORD               = "PASSWORD";
    public static final String USERNAME               = "USERNAME";
    public static final String FIRSTNAME              = "FIRSTNAME";
    public static final String LASTNAME               = "LASTNAME";
    public static final String MAIL                   = "MAIL";
    public static final String SENDNOTIFICATIONS      = "SENDNOTIFICATIONS";
    public static final String TEXTCOLOR              = "TEXTCOLOR";
    public static final String ROLE                   = "ROLE";
    public static final String IMAGE                  = "IMAGE";
    public static final String ALLOW_NOTIFICATIONS    = "ALLOW_NOTIFICATIONS";
    public static final String NEW_TOPIC_NOTIFICATION = "NEW_TOPIC_NOTIFICATION";

    private final Jedis           jedis;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserRepository(Jedis jedis, PasswordEncoder passwordEncoder) {
        this.jedis = jedis;
        this.passwordEncoder = passwordEncoder;
    }

    public AppUser getAppUser(String username) {
        AppUser appUser = new AppUser();

        List<String> values = jedis.hmget(KEY + ":" + username, PASSWORD, USERNAME,
                FIRSTNAME, LASTNAME, MAIL, SENDNOTIFICATIONS, TEXTCOLOR, ROLE, IMAGE,
                ALLOW_NOTIFICATIONS, NEW_TOPIC_NOTIFICATION);

        if (values.get(1) == null) {
            return null;
        }

        appUser.setPassword(values.get(0));
        appUser.setUsername(values.get(1));
        appUser.setFirstname(values.get(2));
        appUser.setLastname(values.get(3));
        appUser.setMail(values.get(4));
        appUser.setSendNotifications(Boolean.parseBoolean(values.get(5)));
        appUser.setTextColor(values.get(6));
        appUser.setRole(UserRoles.valueOf(values.get(7)));
        appUser.setImage(values.get(8));
        appUser.setAllowDataCollection(Boolean.parseBoolean(values.get(9)));
        appUser.setNewTopicNotification(Boolean.parseBoolean(values.get(10)));

        return appUser;
    }

    public void addUser(UserRegistrationForm userRegistrationForm) throws IOException {
        String password = passwordEncoder.encode(userRegistrationForm.getPassword());

        AppUser newUser = new AppUser(userRegistrationForm.getUsername(), password,
                userRegistrationForm.getFirstname(), userRegistrationForm.getLastname(),
                userRegistrationForm.getMail());

        Map<String, String> userDetails = new HashMap<>();
        userDetails.put(USERNAME, newUser.getUsername());
        userDetails.put(PASSWORD, newUser.getPassword());
        userDetails.put(FIRSTNAME, newUser.getFirstname());
        userDetails.put(LASTNAME, newUser.getLastname());
        userDetails.put(MAIL, newUser.getMail());
        userDetails.put(SENDNOTIFICATIONS, Boolean.toString(newUser.isSendNotifications()));
        userDetails.put(TEXTCOLOR, newUser.getTextColor());
        userDetails.put(ROLE, newUser.getRole().toString());
        userDetails.put(IMAGE, newUser.getImage());
        userDetails.put(ALLOW_NOTIFICATIONS, Boolean.toString(newUser.isAllowDataCollection()));
        userDetails.put(NEW_TOPIC_NOTIFICATION, Boolean.toString(newUser.isNewTopicNotification()));

        jedis.hmset(KEY + ":" + newUser.getUsername(), userDetails);
    }

    public void deleteUser(String username) {
        jedis.del(KEY + ":" + username);
    }

    public AppUser updateUser(AppUser appUser) {
        String password = appUser.getPassword();

        AppUser updatedUser = new AppUser();

        updatedUser.setImage(appUser.getImage());
        updatedUser.setRole(appUser.getRole());
        updatedUser.setTextColor(appUser.getTextColor());
        updatedUser.setSendNotifications(appUser.isSendNotifications());
        updatedUser.setMail(appUser.getMail());
        updatedUser.setLastname(appUser.getLastname());
        updatedUser.setFirstname(appUser.getFirstname());
        updatedUser.setPassword(password);
        updatedUser.setNewTopicNotification(appUser.isNewTopicNotification());
        updatedUser.setAllowDataCollection(appUser.isAllowDataCollection());

        Map<String, String> userDetails = new HashMap<>();
        userDetails.put(USERNAME, appUser.getUsername());
        userDetails.put(PASSWORD, updatedUser.getPassword());
        userDetails.put(FIRSTNAME, updatedUser.getFirstname());
        userDetails.put(LASTNAME, updatedUser.getLastname());
        userDetails.put(MAIL, updatedUser.getMail());
        userDetails.put(SENDNOTIFICATIONS, Boolean.toString(updatedUser.isSendNotifications()));
        userDetails.put(TEXTCOLOR, updatedUser.getTextColor());
        userDetails.put(ROLE, updatedUser.getRole().toString());
        userDetails.put(IMAGE, updatedUser.getImage());
        userDetails.put(ALLOW_NOTIFICATIONS, Boolean.toString(updatedUser.isAllowDataCollection()));
        userDetails.put(NEW_TOPIC_NOTIFICATION, Boolean.toString(updatedUser.isNewTopicNotification()));

        jedis.hmset(KEY + ":" + appUser.getUsername(), userDetails);

        return updatedUser;
    }

    public Set<String> getAllUsers() {
        return jedis.keys(KEY + ":*");
    }

    public List<String> getEmailForUser() {
        List<String> mails = new LinkedList<>();

        for (String key : getAllUsers()) {
            mails.add(jedis.hget(key, "MAIL"));
        }

        return mails;
    }

    public String getEmail(String username) {
        return jedis.hget(KEY + ":" + username, MAIL);
    }

    public boolean sendNewTopicNotifications(String username) {
        return Boolean.parseBoolean(jedis.hget(KEY + ":" + username, NEW_TOPIC_NOTIFICATION));
    }

}
