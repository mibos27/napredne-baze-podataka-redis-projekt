package pmf.nbp.redis.model;

import java.util.LinkedHashSet;
import java.util.Set;

public class Topics {

    private Set<String> topics;

    public Topics() {
        this.topics = new LinkedHashSet<>();
    }

    public Topics(Set<String> topics) {
        this.topics = new LinkedHashSet<>(topics);
    }

    public Set<String> getTopics() {
        return topics;
    }

    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

}
