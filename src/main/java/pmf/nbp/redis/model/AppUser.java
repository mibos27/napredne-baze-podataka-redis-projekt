package pmf.nbp.redis.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pmf.nbp.redis.util.Encoder;
import pmf.nbp.redis.util.UserRoles;

public class AppUser implements UserDetails {

    private String username;

    private String password;

    private String firstname;

    private String lastname;

    private String mail;

    private boolean sendNotifications;

    private String textColor;

    private UserRoles role;

    private String image;

    private boolean allowDataCollection;

    private boolean newTopicNotification;

    public AppUser() {
    }

    public AppUser(String username, String password, String firstname, String lastname, String mail)
            throws IOException {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.mail = mail;
        this.sendNotifications = false;
        this.allowDataCollection = false;
        this.newTopicNotification = false;
        this.textColor = "black";
        this.role = UserRoles.USER;
        this.setImage(Encoder.getEncodedString(Encoder.defaultImageToBytes()));
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> auth = new ArrayList<>();
        auth.add(new SimpleGrantedAuthority(role.toString()));
        return auth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isSendNotifications() {
        return sendNotifications;
    }

    public void setSendNotifications(boolean sendNotifications) {
        this.sendNotifications = sendNotifications;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public void setRole(UserRoles role) {
        this.role = role;
    }

    public UserRoles getRole() {
        return role;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isAllowDataCollection() {
        return allowDataCollection;
    }

    public void setAllowDataCollection(boolean allowDataCollection) {
        this.allowDataCollection = allowDataCollection;
    }

    public boolean isNewTopicNotification() {
        return newTopicNotification;
    }

    public void setNewTopicNotification(boolean newTopicNotification) {
        this.newTopicNotification = newTopicNotification;
    }

    @Override
    public String toString() {
        return username.toString();
    }

}
