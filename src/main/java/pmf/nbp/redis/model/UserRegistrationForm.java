package pmf.nbp.redis.model;

import javax.validation.constraints.NotEmpty;

public class UserRegistrationForm {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

    @NotEmpty
    private String matchingPassword;

    @NotEmpty
    private String mail;

    @NotEmpty
    private String firstname;

    @NotEmpty
    private String lastname;

    public UserRegistrationForm() {}

    public UserRegistrationForm(String username, String password, String matchingPassword, String mail, String firstname, String lastname) {
        this.username = username;
        this.password = password;
        this.matchingPassword = matchingPassword;
        this.mail = mail;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

}
