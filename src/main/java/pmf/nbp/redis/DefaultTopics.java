package pmf.nbp.redis;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import pmf.nbp.redis.service.TopicsService;

@Component
public class DefaultTopics implements ApplicationListener<ApplicationReadyEvent> {

    private final TopicsService topicsService;

    @Autowired
    public DefaultTopics(TopicsService topicsService) { this.topicsService = topicsService; }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        try {
            createTopics(
                    List.of(
                            "Hypothetical scenarios",
                            "Cars and automobiles",
                            "Fantasy books",
                            "YA books",
                            "Historical events",
                            "Everyday fun facts",
                            "Construction works",
                            "Games (PC/PS/XBOX)",
                            "New popular songs",
                            "Animal care"
                    )
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createTopics(List<String> topics)
            throws IOException {
        topicsService.addListTopics(topics);
    }
}
