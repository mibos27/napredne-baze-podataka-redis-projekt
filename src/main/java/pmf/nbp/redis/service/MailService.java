package pmf.nbp.redis.service;

import pmf.nbp.redis.model.Mail;

public interface MailService {

    void sendEmail(Mail mail, String path);
}
