package pmf.nbp.redis.service;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pmf.nbp.redis.model.AppUser;
import pmf.nbp.redis.model.Mail;
import pmf.nbp.redis.model.UserRegistrationForm;
import pmf.nbp.redis.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepo;
    private final     MailService     mailService;
    private final PasswordEncoder passwordEncoder;


    @Value("${spring.mail.username}")
    private String mailFrom;

    @Autowired
    public UserServiceImpl(UserRepository userRepo, MailService mailService,
            PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.mailService = mailService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public AppUser getUserByUsername(String username) {
        return userRepo.getAppUser(username);
    }

    @Override
    public boolean userExists(String username) {
        return userRepo.getAppUser(username) != null;
    }

    @Override
    public void createUser(UserRegistrationForm registrationForm) throws IOException {
        if(!userExists(registrationForm.getUsername())) {
            mailConstructor(registrationForm.getMail());
        }
        userRepo.addUser(registrationForm);
    }

    @Override
    public AppUser updateUser(AppUser newUser) {
        return userRepo.updateUser(newUser);
    }

    @Override
    public void deleteUser(String username) {
        userRepo.deleteUser(username);
    }

    @Override
    public AppUser updateImage(String image, AppUser user) {
        user.setImage(image);
        userRepo.updateUser(user);
        return user;
    }

    @Override
    public AppUser changePassword(String password, AppUser appUser) {
        appUser.setPassword(passwordEncoder.encode(password));
        userRepo.updateUser(appUser);
        return appUser;
    }

    public void mailConstructor(String mailTo) {
        Mail mail = new Mail();
        mail.setMailFrom(mailFrom);
        mail.setMailTo(mailTo);
        mail.setMailSubject("Novi korisnik");

        mailService.sendEmail(mail, "newUser.ftl");
    }

}
