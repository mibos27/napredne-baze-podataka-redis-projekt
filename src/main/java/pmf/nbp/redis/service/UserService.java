package pmf.nbp.redis.service;

import java.io.IOException;
import pmf.nbp.redis.model.AppUser;
import pmf.nbp.redis.model.UserRegistrationForm;

public interface UserService {

    AppUser getUserByUsername(String username);

    boolean userExists(String username);

    void createUser(UserRegistrationForm registrationForm) throws IOException;

    AppUser updateUser(AppUser newUser);

    void deleteUser(String username);

    AppUser updateImage(String image, AppUser user);

    AppUser changePassword(String password, AppUser appUser);
}
