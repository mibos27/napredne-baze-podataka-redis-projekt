package pmf.nbp.redis.service;

import pmf.nbp.redis.model.Topics;

import java.util.List;

public interface UserTopicsService {

    Topics getAllUserTopics(String username);

    Topics addOneUserTopic(String username, String topicName);

    Topics addListUserTopics(String username, List<String> topics);

    Long deleteOneUserTopic(String username, String topicName);

    void deleteListUserTopics(String username, List<String> topics);

    Topics getDiff(String username);
}
