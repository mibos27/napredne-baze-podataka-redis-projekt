package pmf.nbp.redis.service;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import freemarker.template.Configuration;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import pmf.nbp.redis.model.Mail;

@Service
public class MailServiceImpl implements MailService{

    private static final Logger LOGGER = LoggerFactory.getLogger(MailServiceImpl.class);

    private final JavaMailSender mailSender;
    private final Configuration  fmConfiguration;

    @Autowired
    public MailServiceImpl(@Qualifier("freeMarkerConfiguration") Configuration fmConfiguration,
            JavaMailSender mailSender) {
        this.fmConfiguration = fmConfiguration;
        this.mailSender = mailSender;
    }

    @Override
    public void sendEmail(Mail mail, String path) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(mail.getMailFrom());
            mimeMessageHelper.setTo(mail.getMailTo());
            mail.setMailContent(getContentFromTemplate(mail.getModel(), path));
            mimeMessageHelper.setText(mail.getMailContent(), true);

            mailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException e) {
            LOGGER.warn("Cannot send message.", e);
        }
    }

    private String getContentFromTemplate(Map<String, Object> model, String path) {
        StringBuilder content = new StringBuilder();

        try {
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(fmConfiguration.getTemplate(path), model));
        } catch (IOException | TemplateException e) {
            LOGGER.warn("Cannot process template into string.", e);
        }
        return content.toString();
    }

}
