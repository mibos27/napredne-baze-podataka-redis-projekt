package pmf.nbp.redis.service;

import java.util.Iterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pmf.nbp.redis.model.Topics;
import pmf.nbp.redis.repository.UserTopicsRepository;

import java.util.List;

@Service
public class UserTopicsServiceImpl implements UserTopicsService {

    private final UserTopicsRepository userTopicsRepo;
    private final TopicsService        topicsService;

    @Autowired
    public UserTopicsServiceImpl(UserTopicsRepository userTopicsRepo, TopicsService topicsService) {
        this.userTopicsRepo = userTopicsRepo;
        this.topicsService = topicsService;
    }

    @Override
    public Topics getAllUserTopics(String username) {
        return userTopicsRepo.getUserTopics(username);
    }

    @Override
    public Topics addOneUserTopic(String username, String topicName) {
        if (topicsService.isMember(topicName)) {
            return userTopicsRepo.addUserTopic(username, topicName);
        }
        throw new RuntimeException("Topic " + topicName + " doesn't exist");
    }

    @Override
    public Topics addListUserTopics(String username, List<String> topics) {
        Iterator<String> iterator = topics.iterator();
        while (iterator.hasNext()) {
            String current = iterator.next();
            if(!topicsService.isMember(current)) {
                iterator.remove();
            }
        }
        return userTopicsRepo.addUserTopics(username, topics);
    }

    @Override
    public Long deleteOneUserTopic(String username, String topicName) {
        return userTopicsRepo.deleteUserTopic(username, topicName);
    }

    @Override
    public void deleteListUserTopics(String username, List<String> topics) {
        userTopicsRepo.deleteUserTopics(username, topics);
    }

    @Override
    public Topics getDiff(String username) {
        return new Topics(userTopicsRepo.getDiff(username));
    }

}
