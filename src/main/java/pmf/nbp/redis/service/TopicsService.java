package pmf.nbp.redis.service;

import java.util.List;
import pmf.nbp.redis.model.Topics;

public interface TopicsService {

    Topics getAllTopics();

    Topics addOneTopic(String topicName);

    Topics addListTopics(List<String> topics);

    Long deleteOneTopic(String topicName);

    void deleteListTopics(List<String> topics);

    boolean isMember(String topicName);
}
