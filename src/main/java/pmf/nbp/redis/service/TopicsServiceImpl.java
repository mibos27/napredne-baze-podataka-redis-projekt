package pmf.nbp.redis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pmf.nbp.redis.model.Mail;
import pmf.nbp.redis.model.Topics;
import pmf.nbp.redis.repository.TopicsRepository;
import pmf.nbp.redis.repository.UserRepository;
import pmf.nbp.redis.repository.UserTopicsRepository;

@Service
public class TopicsServiceImpl implements TopicsService {

    private final TopicsRepository     topicsRepo;
    private final UserTopicsRepository userTopicsRepository;
    private final UserRepository       userRepository;
    private final MailService          mailService;

    @Value("${spring.mail.username}")
    private String mailFrom;

    @Autowired
    public TopicsServiceImpl(TopicsRepository topicsRepo,
            UserTopicsRepository userTopicsRepository, UserRepository userRepository,
            MailService mailService) {
        this.topicsRepo = topicsRepo;
        this.userTopicsRepository = userTopicsRepository;
        this.userRepository = userRepository;
        this.mailService = mailService;
    }

    @Override
    public Topics getAllTopics() {
        return topicsRepo.getTopics();
    }

    @Override
    public Topics addOneTopic(String topicName) {
        newTopicMail(topicName);
        return topicsRepo.addTopic(topicName);
    }

    @Override
    public Topics addListTopics(List<String> topics) {
        for (String topic : topics) {
            if (!isMember(topic)) {
                newTopicMail(topic);
            }
        }
        return topicsRepo.addTopics(topics);
    }

    @Override
    public Long deleteOneTopic(String topicName) {
        deleteFromUserTopics(topicName);
        return topicsRepo.deleteTopic(topicName);
    }

    @Override
    public void deleteListTopics(List<String> topics) {
        for (String topicName : topics) {
            deleteFromUserTopics(topicName);
        }
        topicsRepo.deleteTopics(topics);
    }

    @Override
    public boolean isMember(String topicName) {
        return topicsRepo.topicIsMember(topicName);
    }

    private void deleteFromUserTopics(String topicName) {
        Set<String> allUserTopicsKeys = userTopicsRepository.getAllUserTopics();

        for (String userTopicsKey : allUserTopicsKeys) {
            if (userTopicsRepository.isMember(topicName, userTopicsKey)) {
                userTopicsRepository.deleteWithKey(userTopicsKey, topicName);
            }
        }
    }

    public void newTopicMail(String topic) {
        for (String mail : userRepository.getEmailForUser()) {
            Map<String, Object> model = new HashMap<>();
            model.put("topic", topic);

            mailConstructor("New topic added", model, "newTopic.ftl", mail);
        }
    }

    public void mailConstructor(String subject, Map<String, Object> model, String path, String mailTo) {
        Mail mail = new Mail();
        mail.setMailFrom(mailFrom);
        mail.setMailTo(mailTo);
        mail.setMailSubject(subject);
        mail.setModel(model);

        mailService.sendEmail(mail, path);
    }

}
