package pmf.nbp.redis.util;

import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

public class Encoder {

    public static byte[] defaultImageToBytes() throws IOException {
        File file = new ClassPathResource("static/default-user.png").getFile();
        return Files.readAllBytes(file.toPath());
    }

    public static String getEncodedString(byte[] bytes) {
        String src = new String(Base64.getEncoder().encode(bytes));
        StringBuilder sb = new StringBuilder();
        sb.append("data:image/png;base64,");
        sb.append(src);
        return sb.toString();
    }

}
